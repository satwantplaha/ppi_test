﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PPI_TEST.Logic;

namespace PPI_TEST.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        [HttpPost]
        public async Task<IActionResult> Post([FromQuery] string query = "")
        {
            SearchService searchService = new SearchService();

            var result = await searchService.SearchRemote(query);

            return Ok(result);
        }
    }
}
