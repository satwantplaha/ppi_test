﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPI_TEST.Logic.Models
{
    public class Contact
    {
        public int Id { get; set; }
        public string Office { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Province { get; set; }
        public string Postal { get; set; }
        public dynamic Phone { get; set; }
        public dynamic Coordinates { get; set; }
        public dynamic Tags { get; set; }

        public List<Contact_By_Category> Contacts_By_Category { get; set; }
    }

    public class Contact_By_Category
    {
        public string Category { get; set; }
        public List<Contact_In_Category> Contacts { get; set; }
    }

    public class Contact_In_Category
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public class Searched_Contact
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
