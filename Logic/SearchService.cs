﻿using Newtonsoft.Json;
using PPI_TEST.Logic.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PPI_TEST.Logic
{
    public class SearchService
    {
        static HttpClient _client;

        static readonly Uri remoteUri = new Uri("https://ppipubsiteservices.azurewebsites.net/api/contacts/en");

        // Search function
        // returns different type of list for query (e.g. for office and contact)
        // that's why returning standard object
        public async Task<object> SearchRemote(string query)
        {
            var data = await GetAllData();

            // if no query value return all data
            if (string.IsNullOrWhiteSpace(query))
            {
                return data;
            }

            // for a query first search for office only and return
            var searchedContactsByCategory2 = data.Where(w=>w.Office.Contains(query, StringComparison.InvariantCultureIgnoreCase)).Select(s => s);

            if (searchedContactsByCategory2.Any() == true)
            {
                return searchedContactsByCategory2;
            }

            // if no office data found search for contact name and retrun
            var searchedContactsByCategory3 = data.SelectMany(s => s.Contacts_By_Category);

            var searchedContactsByCategory33 = searchedContactsByCategory3.SelectMany(s => s.Contacts);
                       
            return searchedContactsByCategory33
                .Where(w => w.Name.Contains(query, StringComparison.InvariantCultureIgnoreCase))
                .Select(s => new Searched_Contact
            {
                Email = string.Format("[{0}](mailto:{0})", s.Email),
                Name = s.Name,
                Phone = s.Phone,
                Title = s.Title
            }).GroupBy(g=>g.Name).Select(s=>s.First()).ToList();
        }


        // Function connects to source api and retrieve all data and return List of objects 
        // or empty list on not success
        public static async Task<List<Contact>> GetAllData()
        {
            _client = new HttpClient();

            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await _client.GetAsync(remoteUri);

            _client = null;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonStr = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<Contact>>(jsonStr);
            }
            else
            {
                return new List<Contact>();
            }
        }
    }
}
